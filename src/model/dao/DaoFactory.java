package model.dao;

import control.db.ConexaoDb;
import model.dao.impl.CursoDaoImpl;
import model.dao.impl.OpcaoDaoImpl;
import model.dao.impl.QuestaoDaoImpl;
import model.dao.impl.TarefaDaoImpl;

public class DaoFactory {
	
	public static CursoDao createCursoDao() {
		return new CursoDaoImpl(ConexaoDb.abrirConexao());
	}
	
	public static OpcaoDao createOpcaoDao() {
		return new OpcaoDaoImpl(ConexaoDb.abrirConexao());
	}
	
	public static QuestaoDao createQuestaoDao() {
		return new QuestaoDaoImpl(ConexaoDb.abrirConexao());
	}
	
	public static TarefaDao createTarefaDao() {
		return new TarefaDaoImpl(ConexaoDb.abrirConexao());
	}
}
